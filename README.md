# README #

## Instalar pre-requisitos ##

	$ sudo apt-get install ros-indigo-moveit-*
	$ sudo apt-get install ros-indigo-ros-control
	$ sudo apt-get install ros-indigo-ros-controllers
	$ sudo apt-get install ros-indigo-joint-state-controller
	$ sudo apt-get install ros-indigo-effort-controllers
	$ sudo apt-get install ros-indigo-position-controllers
	$ sudo apt-get install ros-indigo-joint-trajectory-controller
	$ sudo apt-get install ros-indigo-urdf-tutorial 
	
## Descargar paquete robot_scara ##

	$ cd ~/catkin_ws/src
	$ git clone https://marioney@bitbucket.org/marioney/robot_scara.git

## Compilar paquete ##

	$ cd ~/catkin_ws
	$ catkin_make

## Examinar los diferentes archivos URDF ##

	$ roscd robot_scara
	$ cd urdf
	$ gedit 01-scara_base.urdf
	$ roslaunch urdf_tutorial display.launch model:=01-scara_base.urdf

### Repetir lo mismo para los archivos ###
    
    01-scara_base.urdf
    02-scara_shapes.urdf
    03-scara_origins.urdf
    04-scara_materials.urdf
    05-scara_joints.urdf
    06-scara_physics.urdf
    07-scara_parametrized.urdf.xacro
    
### Modelo compatible con gazebo ###
    08-scara_gazebo.urdf.xacro
    
## Revisar configuración de controladores ##

	$ roscd robot_scara
	$ cd config
	$ gedit robot_scara_control.yaml

## Revisar configuración de los lanzadores ##

	$ roscd robot_scara
	$ cd launch
	$ gedit robot_scara_launch.launch
	$ gedit robot_scara_control.launch

## Lanzar robot modelado en gazebo ##

	$ roslaunch robot_scara robot_scara_launch.launch
	
## Comprobar movimiento de las articulaciones ##

	$ rosrun robot_scara move_robot_scara.py
   

## Descargar paquete de configuración de MoveIt!

	$ cd catkin_ws/src
	$ git clone https://marioney@bitbucket.org/marioney/robot_scara_moveit_config.git
	
## Probar el funcionamiento

    $ roslaunch robot_scara_moveit_config robot_scara_gazebo_demo.launch
	
	

